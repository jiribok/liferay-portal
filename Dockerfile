FROM java:7
# Install unzip
RUN apt-get update && apt-get install -y unzip
RUN apt install -y net-tools lynx mysql-client vim nano mc

# Download and install liferay
# Examples
#RUN curl -OL http://downloads.sourceforge.net/project/lportal/Liferay%20Portal/6.2.3%20GA4/liferay-portal-tomcat-6.2-ce-ga4-20150416163831865.zip
#RUN curl -OL http://downloads.sourceforge.net/project/lportal/Liferay%20Portal/7.0.0%20GA1/liferay-portal-tomcat-7.0-ce-ga1-20160331161017956.zip
#RUN unzip liferay-portal-tomcat-7.0-ce-ga1-20160331161017956.zip -d /opt
#RUN rm liferay-portal-tomcat-7.0-ce-ga1-20160331161017956.zip

# Using shared repository
ADD resources/liferay-portal-tomcat-6.1.0-ce-ga1-20120106155615760.zip /opt/liferay.zip
RUN unzip /opt/liferay.zip -d /opt
RUN rm /opt/liferay.zip

RUN ln -s /opt/liferay-portal-6.1.0-ce-ga1 /opt/liferay-portal
RUN ln -s /opt/liferay-portal/tomcat-7.0.23 /opt/liferay-portal/tomcat

# Add configuration files
ADD resources/portal-ext.properties /opt/liferay-portal/portal-ext.properties
ADD resources/portal-setup-wizard.properties /opt/liferay-portal/portal-setup-wizard.properties
ADD resources/setenv.sh /opt/liferay-portal/tomcat/bin/setenv.sh
ADD run.sh /run.sh

# Liferay data will be stored in a separate data volume
VOLUME /opt/liferay-portal/data

# Expose port 8080
EXPOSE 8080

# Set JAVA_HOME
ENV JAVA_HOME /usr/lib/jvm/java-7-openjdk-amd64/

# Execute liferay
#ENTRYPOINT ["/opt/liferay-portal/tomcat/bin/catalina.sh"]
CMD ["/run.sh"]
